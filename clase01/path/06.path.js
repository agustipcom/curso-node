const path = require('node:path')

// barra separadora de carpetas segun OS
console.log(path.sep)

// unir rutas con path.join
const filePath = path.join('content', 'subfolder', 'test.txt')
console.log(filePath)
// con path.join no importa el OS que se use, siempre se unira con la barra correcta

const base = path.basename('/tmp/apc/tmp.txt')
console.log(base)

const filename = path.basename('/tmp/apc/tmp.txt', '.txt')
console.log(filename)

const extension = path.extname('image.jpg')
console.log(extension)
