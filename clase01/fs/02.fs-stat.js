const fs = require('node:fs')

const stats = fs.statSync('./archivo.txt')
fs.stat('./archivo.txt', (err, asyncStats) => {
  if (err) {
    console.log('Error al leer el archivo: ', err)
    process.exit(1)
  }
  console.log('asyncStats: ', asyncStats)
})

console.log('stats: ', stats)
