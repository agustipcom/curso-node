const { readFile } = require('node:fs/promises')

// IIFE - Immediately Invoked Function Expression
;(async () => {
  console.log('Leyendo el primer archivo....')
  const text = await readFile('./archivo.txt', 'utf-8')
  console.log('first text: ', text)

  console.log('Leyendo el segundo archivo....')
  const text2 = await readFile('./archivo2.txt', 'utf-8')
  console.log('second text: ', text2)
})()

/*
// this is the same as the IIFE above
async function init() {
  console.log('Leyendo el primer archivo....')
  const text = await readFile('./archivo.txt', 'utf-8')
  console.log('first text: ', text)

  console.log('Leyendo el segundo archivo....')
  const text2 = await readFile('./archivo2.txt', 'utf-8')
  console.log('second text: ', text2)
}
init()
 */
