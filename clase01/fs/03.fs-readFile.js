const fs = require('node:fs')
const { promisify } = require('node:util')

const readFilePromise = promisify(fs.readFile)

fs.readFile('./archivo.txt', 'utf-8', (err, text) => {
  if (err) {
    console.log('Error al leer el archivo: ', err)
    process.exit(1)
  }
  console.log('text async 1: ', text)
})
readFilePromise('./archivo.txt', 'utf-8').then((text) => {
  console.log('text promise: ', text)
})
fs.readFile('./archivo2.txt', 'utf-8', (err, text) => {
  if (err) {
    console.log('Error al leer el archivo: ', err)
    process.exit(1)
  }
  console.log('text async 2: ', text)
})

console.log('Leyendo el primer archivo....')
const text = fs.readFileSync('./archivo.txt', 'utf-8')
console.log('text: ', text)

console.log('Leyendo el segundo archivo....')
const text2 = fs.readFileSync('./archivo2.txt', 'utf-8')
console.log('text2: ', text2)
