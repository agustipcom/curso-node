// .js -> uses CommonJS as default
// .mjs -> uses ES Modules (emma script modules)
// .cjs -> uses CommonJS

import { sum, sub, mult } from './functions.mjs'

console.log(sum(1, 2))
console.log(sub(1, 2))
console.log(mult(1, 2))
