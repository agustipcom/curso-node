import {
  platform,
  release,
  arch,
  cpus,
  totalmem,
  freemem,
  uptime
} from 'node:os'

console.log('OS information:')
console.log('----------------')

console.log('OS name', platform())
console.log('OS version', release())
console.log('OS CPU architecture:', arch())
console.log('# of logical CPU cores', cpus().length)
console.log('Total Memory: ', totalmem() / 1024 / 1024 / 1024, 'GB')
console.log('Free Memory: ', freemem() / 1024 / 1024 / 1024, 'GB')
console.log('uptime', uptime() / 60 / 60 / 24, 'days')
