# This is a Course of Node from 0

> - **CLASE 1** &rarr; Bases de Node JS: Introducción y módulos
>
> - **CLASE 2** &rarr; Crear una API con Express
>
> - **CLASE 3** &rarr; Desarrollo de API Rest y uso de CORS
>
> - **CLASE 4** &rarr; Deploy a producción, arquitectura MVC y buenas practicas
>
>   - deployment code in Github so I can use FL0 https://github.com/agustipc/rest-api-deploy
>   - API Rest deployed &rarr; https://rest-api-deploy-9sle-dev.fl0.io/movies
>
> - **CLASE 5** &rarr; Bases de datos MySQL
>
>   - Inyección de dependencias para pasar el modelo al iniciar la app
>
> - **CLASE 6** &rarr; Chat Web Sockets
>   - `npm run dev` to start the server
>   - Using Morgan logger extension to see the logs in the console
>   - Using Socket.io to use websockets // could be changed for ws library
>   - Using Turso to create a SQL lite DB
>     - libraries to use Turso: `@libsql/client` and `dotenv` to read the .env file

## Commands

to start the server `node --watch app.js`

drop table messages: our talbes name -> uncommon-spider-woman

```
turso db shell <name of the table>
```

```
DROP TABLE messages;
```
