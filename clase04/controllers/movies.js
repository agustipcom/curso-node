import { MovieModel } from '../models/local-file-system/movie.js'
import { validateMovie, validatePartialMovie } from '../schemas/movies.js'

export class MovieController {
  static async getAll (req, res) {
    // TODO:: try catch on middleware
    try {
      const { genre } = req.query
      const movies = await MovieModel.getAll({ genre })

      res.json(movies)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  static async getById (req, res) {
    try {
      const id = req.params.id
      const movie = await MovieModel.getById({ id })

      if (movie) {
        return res.json(movie)
      }

      res.status(404).json({ error: 'Movie not found' })
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  static async create (req, res) {
    try {
      const result = validateMovie(req.body)

      if (result.error) {
        return res.status(400).json({ error: JSON.parse(result.error.message) })
      }

      const newMovie = await MovieModel.create({ input: result.data })

      res.status(201).json(newMovie)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  static async remove (req, res) {
    try {
      const { id } = req.params
      const result = await MovieModel.remove({ id })

      if (result === false) { return res.status(404).json({ error: 'Movie not found' }) }

      return res.json({ message: 'Movie deleted' })
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  static async update (req, res) {
    try {
      const result = validatePartialMovie(req.body)

      if (!result.success) {
        return res.status(400).json({ error: JSON.parse(result.error.message) })
      }

      const { id } = req.params

      const updatedMovie = await MovieModel.update({ id, input: result.data })

      return res.json(updatedMovie)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }
}
