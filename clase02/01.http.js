const http = require('node:http')
const fs = require('node:fs')

const desiredPort = process.env.PORT ?? 3000

const processRequest = (req, res) => {
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')

  switch (req.url) {
    case '/': {
      res.end('Welcome to the homepage!')
      break
    }
    case '/status-codes': {
      fs.readFile('./StatusCodes.png', (err, data) => {
        if (err) {
          res.statusCode = 500
          res.end('Internal Server Error')
        } else {
          res.setHeader('Content-Type', 'image/png')
          res.end(data)
        }
      })
      break
    }
    case '/contact': {
      res.end('Contact')
      break
    }
    default: {
      res.statusCode = 404
      res.end('404 Not Found')
    }
  }
}

const server = http.createServer(
  processRequest
)

server.listen(desiredPort, () => {
  console.log(`server started on port http://localhost:${desiredPort}`)
})
