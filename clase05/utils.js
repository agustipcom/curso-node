// a way of read JSON in ESModules recommended for now until supported ->        "import movies from './movies.json' with {type: 'module'}"
import { createRequire } from 'node:module'
const require = createRequire(import.meta.url)

export const readJSON = (path) => require(path)
