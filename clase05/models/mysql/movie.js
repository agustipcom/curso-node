import mysql from 'mysql2/promise'

const config = {
  host: 'localhost',
  user: 'root',
  port: 3306,
  password: '',
  database: 'moviesdb'
}

const connection = await mysql.createConnection(config)

export class MovieModel {
  static async getAll ({ genre }) {
    if (genre) {
      const lowerCaseGenre = genre.toLowerCase()

      // get genre ids from database by name
      const [genres] = await connection.query(
        'SELECT id, name FROM genre WHERE LOWER(name) = ?;', [lowerCaseGenre]
      )

      if (genres.length === 0) return []

      // get the id from the first genre result
      // const [{ id }] = genres

      // get all movies ids form datbase table
      // const [movies] = await connection.query('SELECT BIN_TO_UUID(id) id FROM movie')

      // query to movie_genres
      // join
      return []
    }
    const [movies] = await connection.query('SELECT *, BIN_TO_UUID(id) id FROM movie')
    return movies
  }

  static async getById ({ id }) {
    const [movies] = await connection.query('SELECT title, year, director, duration, poster, rate, BIN_TO_UUID(id) id FROM movie WHERE id = UUID_TO_BIN(?);', [id])
    if (movies.length === 0) return null
    return movies[0]
  }

  static async create ({ input }) {
    const {
      genre: genreInput, // genre is an array
      title,
      year,
      duration,
      director,
      rate,
      poster
    } = input

    // tambien puede ser con crypto.randomUUID()
    const [uuidResult] = await connection.query('SELECT UUID() uuid;')
    const [{ uuid }] = uuidResult

    try {
      // el uuid lo podemos poner en VALUES porque lo generamos nosotros, no lo pasa el usuario
      await connection.query(`INSERT INTO movie (id, title, year, director, duration, poster, rate) VALUES(UUID_TO_BIN("${uuid}"), ?, ?, ?, ?, ?, ?);`,
        [title, year, director, duration, poster, rate])
    } catch (error) {
      // no dejar que el user lo vea porque puede ser sensible
      throw new Error('Error creating movie')
      // enviar la traza a un serviio interno
      // sendError(error)
    }

    const [movies] = await connection.query('SELECT title, year, director, duration, poster, rate, BIN_TO_UUID(id) id FROM movie WHERE id = UUID_TO_BIN(?);', [uuid])

    return movies[0]
  }

  static async remove ({ id }) {
    const [result] = await connection.query('DELETE FROM movie WHERE id = UUID_TO_BIN(?);', [id])
    return result.affectedRows > 0
  }

  static async update ({ id, input }) {

  }
}
