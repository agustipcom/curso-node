import { validateMovie, validatePartialMovie } from '../schemas/movies.js'

export class MovieController {
  // we are passing from outside which movieController is using the controller (mysql, mongo, etc) for better transparency
  constructor ({ movieModel }) {
    this.movieModel = movieModel
  }

  getAll = async (req, res) => {
    // TODO:: try catch on middleware
    try {
      const { genre } = req.query
      const movies = await this.movieModel.getAll({ genre })

      res.json(movies)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  getById = async (req, res) => {
    try {
      const id = req.params.id
      const movie = await this.movieModel.getById({ id })

      if (movie) {
        return res.json(movie)
      }

      res.status(404).json({ error: 'Movie not found' })
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  create = async (req, res) => {
    try {
      const result = validateMovie(req.body)

      if (result.error) {
        return res.status(400).json({ error: JSON.parse(result.error.message) })
      }

      const newMovie = await this.movieModel.create({ input: result.data })

      res.status(201).json(newMovie)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  remove = async (req, res) => {
    try {
      const { id } = req.params
      const result = await this.movieModel.remove({ id })

      if (result === false) { return res.status(404).json({ error: 'Movie not found' }) }

      return res.json({ message: 'Movie deleted' })
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }

  update = async (req, res) => {
    try {
      const result = validatePartialMovie(req.body)

      if (!result.success) {
        return res.status(400).json({ error: JSON.parse(result.error.message) })
      }

      const { id } = req.params

      const updatedMovie = await this.movieModel.update({ id, input: result.data })

      return res.json(updatedMovie)
    } catch (error) {
      res.status(500).json({ error: error.message })
    }
  }
}
