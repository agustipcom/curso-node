import express, { json } from 'express'
import { corsMiddleware } from './middlewares/cors.js'

import { createMovieRouter } from './routes/movies.js'

export const createApp = ({ movieModel }) => {
  const app = express()
  app.use(json()) // middleware que parsea el body de las peticiones a JSON
  app.use(corsMiddleware()) // middleware que habilita CORS
  app.disable('x-powered-by') // deshabilita el header X-Powered-By por seguridad

  app.use('/movies', createMovieRouter({ movieModel }))

  app.options('/movies/:id', (req, res) => {
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PATCH')

    res.send(200)
  })

  const PORT = process.env.PORT || 3000

  app.listen(PORT, () => {
    console.log(`Servidor escuchando en http://localhost:${PORT}`)
  })
}
